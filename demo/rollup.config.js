import { nodeResolve } from '@rollup/plugin-node-resolve'
import html from '@web/rollup-plugin-html'
import copy from 'rollup-plugin-copy'

export default {
    input: 'demo/index.html',
    output: { dir: 'dist' },
    plugins: [
        html(),
        copy({
            targets: [
                { src: [ 'demo/*.glb', 'demo/*.hdr', 'demo/*.bhm' ], dest: 'dist/' }
              ]
        }),
        nodeResolve(),
        //terser() -> caused errors with variable names
    ],
}

